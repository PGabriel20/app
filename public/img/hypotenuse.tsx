const css = `
.triangle-container {
	width: 500px;
	margin: auto;
	text-align:center;
}

.triangle-move:hover {
	transform: translate(-16px, 16px);
}

.triangle, .right-angle {
	fill: transparent;
	stroke: blue;
	stroke-width: 4;
	transition: all 0.35s ease-in-out;
	transform-origin: 50px 200px;
}

.hypotenuse-text {
	font-size: 24px;
	transform: rotate(20.5deg);
	fill: blue;
	font-family: 'Roboto Mono',monospace;
}
`

export const HypotenuseSVG = 

<div className="triangle-container">
	<svg height="250" width="500">
		<polygon points="50,50 50,200 450,200" className="triangle" />
		<polygon points="50,50 50,200 450,200" className="triangle triangle-move" />
		<polygon points="50,200 50,175 75,175 75,200" className="right-angle" />
		{/* <text className="hypotenuse-text" x="75" y="15" textLength="80%">hypotenuse</text> */}
		Looks like your browser doesn't support inline SVG, but this should be a right triangle with the text "Hypotenuse"!
	</svg>

	<style>
		{css}
	</style>
</div>