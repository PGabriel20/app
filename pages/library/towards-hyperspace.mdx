---
title: "Towards Hyperspace"
date: '2021-05-12'
author: The Hyperlink Team
toc: true
living: false
tags: [announcement]
description: "Building social environments for flexible learning — our vision for Hyperspace."
topic: ""
---

import {LibraryLayout} from '../../components/Library'
export default ({children}) => <LibraryLayout {...metadata}>{children}</LibraryLayout>

How do we learn with others on the internet? And _where_?

How can we share space for learning; build and inhabit it together?

As internet learning matures, we're collectively rediscovering some simple truths, like:

**Learning is better with people.**

We've tried so many tools and contexts for learning — video calls, forums, chat apps and more. All are useful; none feel like Hyperlink. All feel like powerful tools; none feel like home.

We've reached the conclusion that **we need better social environments for learning.**

## Social spaces for learning

Learning online happens in a mix of different spaces and contexts.

All kinds of content exists in videos, books, websites, courses free and paid, sometimes with ancillary social channels — comment sections; email exchanges with the author.

Learning cohorts often conduct live meetings via video calls (Zoom), and continue conversations with asynchronous forum discussion (Discourse), as well as chat (Discord) and collaborative tools (Google; Figma).

Independent, self-driven learning happens…well, fragmented across the internet! Calls with friends, ad hoc learning groups, personal notes in knowledge management apps, private chats, and so on.

Sometimes the above contexts work quite well. But often, important qualities are hard to find:

- A good balance of sync / async, where you can be present with others but also build on learning over time
- Environments that foster emergence, where work can take shape and gradually yield projects and artifacts
- The feeling of being in a space that's flexible — multimodal, adaptable, interconnected
- Cozy, friendly, welcoming vibes; a place you want to return to
- Support for solo _and_ group learning; structured _and_ exploratory learning
- Helpful scaffolding, the right blend of structure and freedom
- Ways to discover people — and books, fields, ideas — you otherwise wouldn't have encountered

To fill this gap; to address these missing pieces…

**We're building something we call Hyperspace.**

## What is Hyperspace?

We can start by giving many short answers:

Website. Tool. Place. Database. Medium. Massively multiplayer note-taking app. Social network. Internet university.

Above all, Hyperspace is a **space for learning**.

You can use it to organize and structure your own learning, and to connect it to others'. You can use it to represent all sorts of things — notes, events, books, chats, questions, ideas, groups, workshops — the many materials of learning.

You can connect all these things together, creating a rich social and informational environment to explore things together.

Hyperspace is intended to bridge the gaps between solo and group learning; between learning that's exploratory and learning that's directed at specific goals:

- **Group learning** — a cohort of a course or club can meet live in Hyperspace, take notes, create breakout groups, share feedback, build projects over time
- **Solo learning** — use Hyperspace as your digital garden focused on learning, from maintaining personal reading lists, to doing research, and invite others to see what you're up to
- **Exploratory learning** — tinker on personal projects, and gradually open them up to collaborators
- **Directed learning** — share your artifacts; form learning groups to create specific things

Hyperspace is for the messy in between — learning that's open, collaborative, multi-modal, and flexible.

## A space for flexible learning

Effective learning blends self-directed exploration with social interaction.

It builds from individual experiences, and at the same time, benefits from other people. You want to blaze your own trails, and adapt as you go — but you also want to share your work, get feedback, and collaborate. Having access to a wider network makes it more likely that you can find the people best able to connect to your specific context.

To balance these needs, we need learning spaces that stay flexible.

We need a platform that is both scalable and specific. We can't hard code all the different structures and interactions that people may find useful for learning. Instead, we must give learners ways to combine things in ways that help achieve their goals.

## A space for many timescales

People connect in many different ways, over a range of temporal modes, from being together live, to reading someone's writing years after they wrote it.

If you want to support the full spectrum of social structures people need, you need to support multiple timescales.

In Hyperspace there are (roughly) three:

- In the slowest mode, you communicate through documents. You can read what other people have written, explore, and write things yourself.
- In the middle is the _log_. You can subscribe to updates from a document's log, which will give you a stream of information you can dip into whenever you want. When people broadcast, it is in a "timely" context, but may be seen at different times by different people.
- In the most real-time mode, you can join a voice call, and talk together with others in a document, live.

All of these things are centered around the same basic entity, a document or "space", and can co-exist in that space. So a single space can support slowly growing content, frequent updates in the log, and synchronous voice calls.

## A space for intention

Learning is about more than just collecting information. You have to _do_ something with that information. Hyperspace is a tool for not just gathering information, but engaging with it in structured ways.

So, too, with social relationships. Communities aren't just places to collect people. People have to _do_ things together. Hyperspace is a tool for people to intentionally engage with each other.

By engaging with people in different ways, across different timescales, we can gradually build trust and allow for the growth of collaborative relationships.

We can start with exploratory tinkering, and direct our learning towards specific goals, whether personal or together with others. By aligning around shared goals, we learn in more powerful ways.

## A space that's a place

Too often, learning online doesn't really happen anywhere.

In the same way a cafe can serve as a third space, outside of home and work environments, Hyperspace is a specific destination for learning that's rewarding to return to.

It's designed to radiate good vibes, cultivate a welcoming atmosphere, and invite you to stay a while.

It's also meant to feel like a place to make your own — a workshop, library, and laboratory where you can not only browse but build.

## Where is Hyperspace?

Hyperspace is in development, and we're gearing up for a semi-public alpha.

We're beginning to shape some general Hyperlink spaces, and use it as a playground for our own learning. And we're about to start inviting others in to test and play around with us.

In the coming weeks, we'll share more details and open Hyperspace for use for events, courses, learning clubs, and further beta testing.

**If you're the early adopter type and this vision speaks to you, let us know!**
