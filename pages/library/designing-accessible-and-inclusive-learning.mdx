---
title: Designing Accessible and Inclusive Learning
date: '2021-07-14'
author: Brendan
toc: true
living: true
tags: [learning-design]
description: "Things to consider in designing learning experiences that work well for as many people as possible."
# topic: ""
---

import {LibraryLayout} from '../../components/Library'
export default ({children}) => <LibraryLayout {...metadata}>{children}</LibraryLayout>

As we work to create great learning experiences, it's important to consider how we can make those experiences open to, and effective for, as many people as possible.

This includes considerations around disability, and making sure learners can access course materials and participate in the learning experience. It includes encouraging diverse learners, and working actively to make a course feel warm and welcoming to all.

It includes how you design the content of your course, its overall structure, and its group activities. And it includes, as well, how you talk about that course; how you approach its pricing and marketing, its syllabus design and outreach strategy.

We'll no doubt encounter challenges and constraints that affect these things, both with particular courses and with online learning generally. Learning design is an iterative process; we won't get everything right at first. But the more thoughtful we are about these things, the more intentional we can be about improving how we teach and learn.

## Accessibility

How can you design a learning space that's accessible to as many students as possible, including students with different learning styles, or with disabilities? Some things to consider:

### Video

Are you assigning students to watch particular videos? Check to make sure those videos are captioned correctly.

For live video calls in your course, can you use video software that supports live captions for participants who may need it? Google Meet and Zoom both have limited support for live captions (English-only; variable accuracy). It's also helpful to record live sessions so that folks who are unable to attend can catch up later on their own schedule.

You might also consider how to provide essential material in other forms, for example sharing slides and written assignment details, and having participants generate shared notes from discussions. Providing information in more ways than one, including ways to engage asynchronously, can be helpful for everyone.

### Course Resources

Do your best to ensure that websites, PDFs, or other required readings are accessible, or that you provide that material in another format.

Sometimes you can't get around requiring paid tools, readings, or other material. But when possible it's worth providing easy access to (for example) excerpted pieces or freely available online readings, as opposed to expensive textbooks or closed-access journals.

You can also think about ways to bring in diverse readings, references, examples, and other source material, making sure it's not just (for example) readings that are all by white, male authors. Including many types of voices and cultures is a great way to invite learners to do likewise, and expand the range of conversations you're comfortable having together.

### Web Accessibility

There are many things that help in making the internet more accessible, from high contrast fonts to responsive design to semantic HTML. If you're making your own web resources, try to keep accessible design in mind, and use services / platforms that do as well.

We try to make the Hyperlink platform itself accessible, though much of what we're building is beta stage and we know we have lots of room for improvement. If you or your course participants encounter any issues using the site, please let us know and we'll do our best to address.

Resources:

- Surprisingly good [accessibility overview](https://docs.microsoft.com/en-us/windows/apps/design/accessibility/accessibility-overview) document from Microsoft
- [Accessibility Tools and Resources](https://webaim.org/resources/) from WebAIM
- [Chrome's Lighthouse](https://developers.google.com/web/tools/lighthouse) is a popular (but not perfect) accessibility checker

### Flexible Learning

Think about how you might be able to support different styles and modes of learning in the course. This might mean making key resources available in written form as well as delivered by video, or designing a variety of exercises that give participants room to shine in reflective work as well as active conversation.

You might also consider things like time constraints, and how to give people flexibility in how they do assignments, whether different formats or mediums, options for collaborating with other learners, or ways to scale an artifact in complexity.

## Inclusivity

How can courses, clubs, and events provide an environment that's inclusive and equitable for learners?

To start, it's useful to be aware of assumptions and attitudes — yours, and those of learners — that you may be bringing to the table.

Creating a welcome environment and making people feel like they belong is something you do in the course itself, but also well before it starts, in the design of the syllabus and the messaging around it.

### Course Description

Language matters. The tone of how you talk about the course, the way you frame who it's for and what participants will get out of it — these can all affect how inclusive a course feels.

There's a balance here. You do want to accurately describe the goals of a course, any prerequisite experience, and other things that may help learners self-select and identify the course as a good fit — but you don't want to unintentionally put off people who might be great participants, but can't see themselves in the course description.

Resources:

- [Self-Defined](https://www.selfdefined.app/), an open source community dictionary of inclusive language
- Geek Feminism's [Code of Conduct](https://geekfeminismdotorg.wordpress.com/about/code-of-conduct/) and [guide to nonsexist language](https://geekfeminism.wikia.org/wiki/Nonsexist_language)

### Outreach

You should be thoughtful and intentional about how you promote a course, too. This includes not only how you talk about it, but where — what audiences are you close to already, and how might you reach learners beyond your existing networks?

It's a good idea to consider how your own network might limit your reach, and work to get the word out more widely, to people in other areas, communities, or demographics who might benefit from the course.

### Warm Welcome

Particularly at the beginning of a course, it's important to communicate clearly and set a welcoming tone.

It can be useful to start with introductions and icebreakers that help participants get to know one another and begin to establish trust. As part of this welcome, you may want to give people a chance to share pronouns, talk a bit about themselves and their goals for the course, and maybe do something playful together.

This is also a good time to clarify important things about the course — not everyone is as familiar with the syllabus as you are, and it doesn't hurt to repeat key details and make sure everyone's on the same page.

### Individual Attention

Can you build in elements of your course that allow for personal attention, to address the particular needs and goals of individual learners? The better you're able to understand learners' goals and interests, the better you can help them connect the course material to their own lives in personally relevant and impactful ways.

Some examples might include scheduling office hours or casual sessions where students can ask questions, making it clear they can message you with questions, and surveying learners about their goals prior to the start of the course.

### Collaborative Modes

You might also consider designing a variety of forms for participation and collaboration among learners, ranging from larger group discussions, to smaller breakout groups, to pairing people up 1:1 for feedback or other exercises. Some may work better for different learners, so it can be useful to have a mix.

Encouraging learners to be curious and playful, to ask questions, to share openly and to give feedback generously, can help get everyone more comfortable learning together.

### Financial Considerations

Great learning experiences are valuable, and we believe teachers and facilitators should be sustainably compensated for creating such experiences.

It is, however, important to consider that learners may be coming from very different background and financial situations, and worth thinking about ways to make paid learning more equitable.

One good way to do this is to reserve a certain number of spots in a cohort for scholarship rates, and offer a heavily subsidized rate to a few participants who most need it. This can be done on Hyperlink with discount codes; you can mention in a course or club description, and invite interested learners to reach out if they're interested.

## Learner's Code

On Hyperlink, we're all about collaborative, co-created learning, where people come together around shared curiosity and explore things together.

We have a [Learner's Code](https://hyperlink.academy/manual/learners#learners-code), outlining expectations for everyone on Hyperlink (teachers, facilitators, peers co-creating a club — we're all learners).

To summarize, this includes: cultivating beginner's mind, leaving biases at the door, giving your full attention and participation, communicating clearly, and learning to be vulnerable with each other.

If you're creating a course or club, you might want to add some additional guidelines of your own, but we think these are a solid starting point for setting shared expectations.

Though our moods and energy levels may shift day to day, keeping these things in mind can help us remember to be open-minded, generous, encouraging, and kind to one another as we learn together.

---

*Thanks to Stephen for initial thoughts and encouragement in drafting this, Sal and Jake for further suggestions, and our friends on Twitter for adding some good ideas as well!*
