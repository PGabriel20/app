---
title: The Art of Gathering
date: '2021-05-19'
author: Brendan
toc: true
living: true
tags: [bookshelf]
description: "Exploring how we design gatherings, and intentionally shape social contexts for meaningful learning."
topic: "https://forum.hyperlink.academy/t/book-review-the-art-of-gathering/2896"
---

import {LibraryLayout} from '../../components/Library'
export default ({children}) => <LibraryLayout {...metadata}>{children}</LibraryLayout>

## The art of gathering for learning

We've been thinking a lot about the social contexts of learning, and how people come together to explore something in a directed way.

Learning with people is a foundational principle of Hyperlink, and a particular area of focus lately as we prepare for the initial beta launch of our social learning space, [Hyperspace](https://hyperlink.academy/library/towards-hyperspace).

I enjoyed Priya Parker's [The Art of Gathering](https://www.goodreads.com/book/show/37424706-the-art-of-gathering) — about hosting good events, generally — so I thought it would be a great book to reexamine more specifically in the context of online learning groups!

I'll highlight key lessons from the book, and then consider how they may be relevant to learning on the internet.

For "gathering", you can read "course", or "book club", or any other collaborative learning experience.

## A gathering should have a purpose

Parker notes that it's all too easy to end up with a vaguely identified purpose, or one where we bring general assumptions rather than something special:

> Just as many of us assume we know what a trial is for, so we think we know what a birthday party is for, or what a wedding is for, or even what a dinner party is for. And so our personal gatherings tend not to serve the purposes that they could.

Instead, we can get specific, design for a timely context, and identify clear goals. In designing meaningful gatherings, getting particular — even weird — can go a long way towards making them feel special.

> What are the ingredients for a sharp, bold, meaningful gathering purpose? Specificity is a crucial ingredient. The more focused and particular a gathering is, the more narrowly it frames itself and the more passion it arouses.

When defining the purpose of a gathering, it can be useful to consider how you might take some risks rather than trying to cater to everyone. It's also important to focus on outcomes, transformations — how you want people to be changed by an experience.

> The purpose of your gathering is more than an inspiring concept. It is a tool, a filter that helps you determine all the details, grand and trivial. To gather is to make choice after choice: place, time, food, forks, agenda, topics, speakers. Virtually every choice will be easier to make when you know why you’re gathering, and especially when that why is particular, interesting, and even provocative.

### Applied to learning…

The obvious takeaway is that every course or learning event should have a goal, and it's important to get as specific about this as we can.

To take it a bit further, I think it's important we recognize how "learning" is a large bucket, and the types of goals that a particular course or workshop may emphasize can be very different!

You might have in mind a specific project for participants to complete. But the goal may also be a transformation, or a habit formed. Or your goals may be explicitly social, e.g. joining a community.

## A gathering should be the right size

Group size has strong bearing on how conversation and interactions in a gathering will feel.

Parker gives a helpful cheat sheet of inflection points in the size of groups, and what works nicely (or doesn't) at each:

- Groups of 6 are most intimate, conducive to "high levels of sharing, and discussion through storytelling"
- Groups of 12-15 are "small enough to build trust and intimacy", but also "large enough to offer a diversity of opinion" and a certain degree of "constructive unfamiliarity"
- Groups of 30 start to "feel like a party", with a buzz and "sense of possibility" (and getting too big for a single conversation, though not impossible)
- Groups of 150 or so are an ideal small conference size, where "intimacy and trust is still palpable at the level of the whole group, and before it becomes an audience"

### Applied to learning…

There's certainly a wide range of group sizes that can work for online learning — at Hyperlink the lower to middle range is typical, but it's possible to have meaningful learning experiences with 2-3 people, as well as in cohorts with hundreds.

I agree with Parker when she notes that for gatherings where "lively but inclusive conversation" is a key component, groups of around 8-12 people are usually best. And for larger cohorts, splitting into more intimately sized breakout groups for conversations can work nicely.

## A gathering should have a venue

And that venue should be tied to the event's purpose, not selected based on what's logistically easiest.

An interesting thing Parker notes: "venues come with scripts" — patterns of expected behavior we follow in particular places (e.g. beach vs. boardroom) — and it's useful to be intentional about how a venue can match the purpose bringing people together.

One great point she brings up is around how the design of space can help "challenge traditional hierarchies of teaching and learning", for example by shaping the space you're in to invite participation or collaboration.

Venues can be levers for behavior. Putting people in unfamiliar places can help them break out of their habits. Establishing a perimeter to contain the energy of the gathering, and help create an "alternate world":

> It can be as simple as putting down a blanket for a picnic rather than sitting on the endless expanse of grass; or temporarily covering the glass walls of a fishbowl conference room with flip-chart paper to create a modicum of privacy. Or if there’s an extra chair at a meeting that is not going to be used, removing it and closing the gap between people.

You can also switch it up, moving between spaces for different parts of an event, which can "help people remember different moments better".

### Applied to learning…

Often we don't think of online spaces as "venues", or places in any real sense, yet we all know how different it can feel to collaborate in different tools, and we've all had experiences of different game worlds or other virtual spaces giving very different feels.

We can go a lot further in shaping our spaces for learning. It's also worth considering how we can move between different spaces!

We see this in rudimentary ways e.g. hopping in and out of breakout rooms, or going from video chat conversation to collaborative doc. I think it's common to plan out a session with a detailed outline of how you'll move from lecture to discussion to activities and so on. We should think more about how to match these transitions with different spatial contexts.

## A gathering should be hosted with generous authority

A gathering needs a host — a leader, shaping things and wielding power.

There's such a thing as being too chill — in fact, Parker believes that "chill is a miserable attitude when it comes to hosting gatherings.

The main reason is that you don't want to leave guests to random interactions, where others may direct things in a way that doesn't fulfill your event's purpose.

So, better to take charge, and govern your gathering with "generous authority", strong yet selfless — "imposing in a way that serves your guests", in service of three goals: to protect your guests, temporarily equalize them, and connect them to one another.

A great example Parker shares:
> One way Abousteit helps her guests connect is by priming them to take care of one another. When she gathers a large group of people who are sitting at separate tables, she assigns roles to a guest at each table, which gives them something to do and an excuse to talk to the others around them. A “Water Minister” ensures that everyone has full glasses of water. A “Wine Minister” keeps the wine flowing.

### Applied to learning…

It's really interesting to think about how to shape gatherings intentionally, as a leader, even when that means being a bit bossy. For learning experiences, this often means setting the tone, introducing people to each other, helping break the ice and get people comfortable.

One thing this calls to mind is the role of teacher vs. facilitator — how these roles overlap, and in what cases they're different. I think it's common in many cases to approach the design of a learning experience as something that should be co-created by all participants. This can work well, and it's natural to want to soften the line between teacher and learner, but it can be a lot to balance.

When coming together in a class, a teacher can work to shape the experience, create ground rules, and control the flow of the learning sessions, even while also building in space for learners to contribute and have a say in how the class unfolds.

In a peer learning group, where there may be a facilitator but less clear authority, it can be useful to rotate that role, or at least parts of it, rather than cede it entirely to the group at large, where you risk a lack of focus.

## A gathering should "transport us to a temporary alternative world"

I love this one! The main idea: you can make a gathering less bland, by "designing it as a world that will exist only once".

A specific interesting mechanism Parker proposes is using pop-up rules, rather than etiquette, to shape gatherings. What's the difference?

> In an etiquette-based gathering, the ways of behaving flow from your identity and define who you are. In a rules-based gathering, the behaviors are temporary. Whereas etiquette fostered a kind of repression, gathering with rules can allow for boldness and experimentation. Rules can create an imaginary, transient world that is actually more playful than your everyday gathering. That is because everyone realizes that the rules are temporary and is, therefore, willing to obey them.

Basically, these shared rules, temporarily adopted, lend us to suspend disbelief and inhabit a shared world together. They can also be a great way to get people comfortable taking risks, trying on different masks, experimenting with behaviors they might not otherwise choose.

Parker describes a great example, where she and her husband created the idea of "I Am Here" days where friends come together for a full day to explore a new neighborhood on foot. A simple set of rules:

> If you’re going to join an “I Am Here” day, be there from start to finish (all 10–12 hours). Turn off technology (unless it directly relates to the day). Agree to be present and engaged in the group and what’s going on. One conversation at meals. Be game for anything.

### Applied to learning…

I think there's a lot of interesting space to explore in applying shared social rules to a learning context.

Going back to the point about purposeful venues and spatial transitions — one of the most important transitions is entering the space of learning, and consciously shifting mindset for the duration of a session in specific ways.

Playfulness, presence, and openness are key here. While a learning experience may be structured and guided in specific ways, a facilitator will do well to leave space for improvisation and exploration, creating rules that aren't inhibitive but rather serve as generative constraints.

## A gathering should begin before it begins

A non-obvious insight: "your gathering begins at the moment your guests first learn of it".

This reminds me of Jesse Ball's great [essay about the art of syllabus design](https://bombmagazine.org/articles/on-the-creation-of-syllabi/), and how a syllabus can serve as a manifesto, or beacon, telegraphing the personality of a course and attracting learners who share your vibes.

> The intentional gatherer begins to host not from the formal start of the event but from that moment of discovery. This window of time between the discovery and the formal beginning is an opportunity to prime your guests. It is a chance to shape their journey into your gathering.

The more priming you're able to do, the less you'll have to handle those logistics during the gathering itself. And it doesn't have to be complex; an email can do the trick —

> Priming can be as simple as a slightly interesting invitation, as straightforward as asking your guests to do something instead of bring something.

By asking guests to contribute ahead of time, we can offer an invitation that primes them, sets the tone, and sustains their excitement. Here's a great example:

> Over the years, Abramović has developed the so-called Abramović Method for Music, which includes a way of preparing her guests for these performances. When audience members arrive, they are asked to put all their belongings (including their cellphones) into a locker before entering the venue. Then they sit in a chair silently, wearing noise-canceling headphones for thirty minutes to block out all the distractions that keep us from being truly present. She thinks of this time as a palate cleanser. “The silence is something that prepares them for their experience,” she told me.

### Applied to learning…

Having participants of a course, club, or event prepare something ahead of time can be a powerful way to jump right in and have a basis for meaningful interactions right from the start.

This can get learners invested, sometimes in a more personal way — for example you might have participants read something and reflect on how it applies to their lives, or bring something for show and tell, or giving a special intro.

It works a bit differently with e.g. a single session workshop vs. a longer cohort, but the general principle seems useful in both contexts. Even for a longer cohort, it always takes time for trust to build, and this can help get there sooner.

## A gathering should be closed with care

It's important to accept that every gathering has an end, and it's something you must prepare for.

> When done well, openings and closings often mirror one another. Just as before your opening there should be a period of ushering, so with closings there is a need to prepare people for the end. This is not ushering so much as last call.

I like this framing:
> A strong closing has two phases, corresponding to two distinct needs among your guests: looking inward and turning outward. Looking inward is about taking a moment to understand, remember, acknowledge, and reflect on what just transpired—and to bond as a group one last time. Turning outward is about preparing to part from one another and retake your place in the world.

This can take the form of a special closing session, final reflection, or shared retrospective — some way to allow people to share openly, demonstrate their full complexity, and take the experience and transformation away with them.
> Part of preparing guests for reentry is helping them find a thread to connect the world of the gathering to the world outside.

### Applied to learning…

We emphasize the importance of a group retrospective reflection, but it's interesting to consider other aspects of how a learning experience ends.

Another key closing concept for learning is coming away with some kind of tangible artifact, some concrete evidence of the learning experience, that can be used or built on further in the future.

I think it's also important to consider the group, the relationships that may have formed during the experience, and how people can stay in touch or continue exploring together outside of the formal boundaries of the course or workshop. Ending as possible beginning; invitations to continue…

## The future of learning together online

I first read this book when I was hosting small group dinner parties / show and tell events in my living room, and curious to think more deeply about ways to host with care.

It's been fun to revisit these ideas in light of what we've learned with Hyperlink so far, with an eye towards not only learning, but learning online, where participants may be meeting only virtually. Gatherings centered around learning have a compelling shared context built in, that can work in some special ways.

While the internet presents certain constraints and challenges in how we gather and learn together, it also offers some incredible possibilities — in bonding over niche interests, connecting across vast distances, exploring things in a range of virtual spaces, on a mix of time scales.

As we continue building Hyperlink, and working to make Hyperspace effective as a shared context for people to learn together, we'll keep these lessons in mind.
