import h from "react-hyperscript";
import styled from "@emotion/styled";

import { Box } from "../components/Layout";
// import { Separator } from '../components/Layout'

// import NewsletterSignup from '../components/NewsletterSignup'
import NewsletterSignupMinimal from "../components/NewsletterSignupMinimal";

// import {Calendar, Pencil} from 'components/Icons'
import { HypotenuseSVG } from "../public/img/hypotenuse";
// import {ReactComponent as HypotenuseSvg} from '../public/img/hypotenuse.svg'

// import { ThreeColumn, FourColumn, Separator } from '../components/Layout'
import { colors } from "components/Tokens";
import { Mobile, Tablet } from "components/Tokens";
// import { Primary, Secondary } from '../components/Button'

const Hypotenuse = () => {
  return h(Box, { gap: 32 }, [
    h(Box, { gap: 16, style: { textAlign: "center" } }, [
      h(Title, "Hyperlink Hypotenuse"),
      h(Tagline, "Dispatches from the future of indie internet education."),
      HypotenuseSVG,
    ]),

    h(
      "p.big",
      `Our mission is to build an interdisciplinary space, by and for the internet, pushing forward the frontiers of independent, alternative learning.`
    ),
    h("p.big", [
      `Hypotenuse is our weekly newsletter (`,
      h("em", `Hyperletter`),
      `!) about what we're up to and how you can get involved.`,
    ]),

    h("h3", `What you can expect:`),

    h(FeatureGrid, [
      h(Feature, [
        h("h4", `Launch Announcements`),
        h(
          "p.textSecondary",
          `Be the first to hear about new courses and clubs in our monthly Knowledge Drops`
        ),
      ]),
      h(Feature, [
        h("h4", `Curated Resources`),
        h(
          "p.textSecondary",
          `Interesting links, book reviews, and other fascinating things we find about learning and pedagogy`
        ),
      ]),
      h(Feature, [
        h("h4", `Community Digest`),
        h(
          "p.textSecondary",
          `Events, course demos, updates from Hyperspace (our social learning space — coming soon!)`
        ),
      ]),
      h(Feature, [
        h("h4", `Behind the Scenes`),
        h(
          "p.textSecondary",
          `Prompts, experiments, ideas we're working through, and musings on building Hyperlink`
        ),
      ]),
    ]),

    h("h3", `Drop your email below, and we'll keep you in the loop!`),
    h(
      "p.big",
      `Each week of the month will typically be a different theme (e.g. launch one week, community digest the next).`
    ),
    h(
      "p.big",
      `We'll also send you a welcome email right away, with a few favorite Library links, plus next steps for teaching and learning with Hyperlink.`
    ),

    //SIGNUP FORM
    h(Box, { style: { width: 360, margin: "auto", paddingTop: 32 } }, [
      h(NewsletterSignupMinimal, { tags: ["hypotenuse"] }),
    ]),
  ]);
};

export default Hypotenuse;

/* Text Styling for Landing Content */
const Title = styled("h1")`
  font-family: "Roboto Mono", monospace;
  font-size: 3rem;
  text-decoration: none;

  ${Tablet} {
    font-size: 2.625rem;
  }
`;

const Tagline = styled("h3")`
  font-weight: normal;

  ${Mobile} {
    padding-top: 16px;
  }
`;

/* Benefits - copied from "Feature" and "FeatureGrid" in /create page */
const FeatureGrid = styled("div")`
  display: grid;
  grid-template-columns: 1fr 1fr;
  grid-gap: 32px;

  ${Tablet} {
    grid-template-columns: 1fr 1fr;
  }

  ${Mobile} {
    grid-template-columns: 1fr;
    grid-gap: 16px;
  }
`;

const Feature = styled("div")`
  background-color: white;
  border: solid 1px;
  border-radius: 2px;
  border-color: ${colors.grey80};
  padding: 16px;
`;
