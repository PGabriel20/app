import h from "react-hyperscript";
import styled from "@emotion/styled";

// import {css} from '@emotion/core'

import { Box } from "../components/Layout";
import { Mobile, Tablet } from "components/Tokens";

import NewsletterSignupMinimal from "../components/NewsletterSignupMinimal";

const NewLanding = () => {
  return h(NewLandingWrapper, [
    h(Box, { gap: 32 }, [
      h(Title, ["Hyperlink is making something new 🌱"]),
      h(Box, { gap: 16 }, [
        h(Tagline, [
          `Imagine: a garden — workshop — playground — for internet creators to…`,
        ]),
        h("ul", { style: { paddingLeft: 8 } }, [
          h("li.big", { style: { marginBottom: 8 } }, [
            h("strong", `Work & learn in public:`),
            ` share projects online; shape your profile`,
          ]),
          h("li.big", { style: { marginBottom: 8 } }, [
            h("strong", `Build scenes & squads:`),
            ` explore with friends; find collaborators`,
          ]),
          h("li.big", [
            h("strong", `Focus attention & energy:`),
            ` iterate; do meaningful long-term work`,
          ]),
        ]),
        h("p.big", [
          `Evolving from our experiments with `,
          h(
            "a",
            { href: "https://hyperlink.academy/courses" },
            `clubs and courses`
          ),
          ` and `,
          h("a", { href: "https://space.hyperlink.academy/" }, `hyperspace`),
          `…`,
        ]),
        h(
          "p.big",
          `We're building a social network of interlinked, card-based activity spaces for learning and creating.`
        ),
        h("p.big", [
          `Think `,
          h("strong", `Twitter`),
          ` meets `,
          h("strong", `HyperCard`),
          ` meets `,
          h("strong", `studios and makerspaces`),
          `.`,
        ]),
      ]),
      // h(Separator),
      h(FancySeparator),
      h(Box, { gap: 16 }, [
        h(Tagline, [
          `We want more from the tools we use and the systems we inhabit.`,
        ]),
        h(
          "p.big",
          `We learn across lifetimes, in complex and evolving ways. Reading groups and writing projects; big questions and personal challenges; conversations with friends.`
        ),
        h(
          "p.big",
          `We know the internet is more than a medium for knowledge and experience. At its best it's also a living ecosystem for deep inquiry, creative risk, expressive play.`
        ),
        h(
          "p.big",
          `We dream of a place where we can play with ideas and intentions, mold them into meaningful shapes, and amplify their vital energy together.`
        ),
      ]),
      // h(Separator),
      h(FancySeparator),

      // h(Box, { gap: 16 }, [
      // 	h(Tagline, [
      // 		`Here's a picture of what you might do on Hyperlink one day soon:`
      // 	]),
      // 	h(ThreeColumn, {style:{marginTop:'16px'}}, [
      // 		h(Box, { gap: 8 }, [
      // 			h('h3', `1. Set up your personal studio`),
      // 			h('p.big', `create custom workspaces for meaningful activities, with decks of cards representing ideas and actions, alongside a chat and timeline`),
      // 		]),
      // 		h(Box, { gap: 8 }, [
      // 			h('h3', `2. Explore the neighborhood`),
      // 			h('p.big', `visit others' studios and join open activity spaces — discover inspiration and collaborative possibilities across a range of contexts, with friends old and new`),
      // 		]),
      // 		h(Box, { gap: 8 }, [
      // 			h('h3', `3. Play, maintain, remix, evolve`),
      // 			h('p.big', `support each other in our most meaningful goals — create shared space for ambitious long-term work, becoming our best selves together`),
      // 		]),
      // 	]),
      // ]),

      // h(Separator),
      h(Box, { gap: 16 }, [
        h(
          "p.big",
          `Right now, we're designing Hyperlink to be particularly good for:`
        ),
        h("ul", { style: { paddingLeft: 8 } }, [
          h("li.big", { style: { marginBottom: 8 } }, [
            h("strong", `Creating clubs:`),
            ` spin up learning circles, small groups + big ideas`,
          ]),
          h("li.big", [
            h("strong", `Social projects:`),
            ` organize your work, share, hype each other up`,
          ]),
        ]),
        // h('p.big', [`Check out our `, h('a', {href:'/TK'}, `wireframes`), ` and let us know if you have thoughts!`]),
        h(
          "p.big",
          `We're building with intention, slowly inviting folks to experiment and play with us :)`
        ),
        h(
          "p.big",
          `If this vision resonates, please drop your email for updates and we'll roll out alpha invites as soon as we can ✨ ⟱`
        ),
      ]),

      //SIGNUP FORM
      h(Box, { style: { width: 360, margin: "auto", paddingTop: 32 } }, [
        h(NewsletterSignupMinimal, { tags: ["v2-alpha"] }),
      ]),

      h("p.big", `More soon!`),
      h("p.big", `—Jared, Celine, Brendan`),
    ]),
  ]);
};

export default NewLanding;

const NewLandingWrapper = styled("div")`
  padding: 64px 128px;
  background-color: white;
  border-radius: 5px;
  box-shadow: -2px 2px 20px 0px rgb(0 0 0 / 25%);

  //   max-width: 1024px;
  max-width: 980px;
  margin: 64px auto;
  box-sizing: border-box;

  ${Tablet} {
    padding: 32px;
    margin: 32px;
  }

  ${Mobile} {
    padding: 32px;
    margin: 32px;
  }
`;

const Title = styled("h1")`
  font-family: "Roboto Mono", monospace;
  font-size: 2rem;
  text-decoration: none;
  font-weight: bold;
  z-index: 2;

  ${Tablet} {
    font-size: 1.8rem;
  }
`;

const Tagline = styled("h3")`
  font-weight: normal;
  font-style: italic;
  font-size: 1.5rem;

  ${Tablet} {
    font-size: 1.4rem;
  }
`;

const FancySeparator = styled("img")`
  margin: 16px auto;
  content: url("/img/newLanding/border-sparkle.png");
  // content:url("/img/newLanding/border-vines.png");
  // content:url("/img/newLanding/seedling-icon.png");

  ${Mobile} {
    max-width: 100%;
  }
`;
